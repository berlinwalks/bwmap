import alias from "@rollup/plugin-alias";
import css from "rollup-plugin-import-css";
import nodeResolve from "@rollup/plugin-node-resolve";
import terser from "@rollup/plugin-terser";
import url from "@rollup/plugin-url";

export default Object.freeze({
    input: "js/gpxmap.js",
    output: {
        file: "dist/gpxmap.js",
        sourcemap: true
    },
    plugins: [
        alias({
            entries: [{
                find: /^leaflet$/,
                replacement: "leaflet/dist/leaflet-src.esm.js"
            }]
        }),
        nodeResolve(),
        css({output: "gpxmap.css"}),
        url({fileName: "[dirname][name][extname]", limit: 0}),
        terser()
    ]
});
