#!/usr/bin/env node

/*jslint
    node
*/

/*property
    argv, bbox, bounds, coordinates, create, createWriteStream, date, dates,
    desc, distance, distances, ele, encoding, end, error, exit, features, floor,
    forEach, geometry, indexOf, join, keys, length, link, map, metadata, name,
    parse, prettyPrint, properties, push, readFileSync, reduce, round, slug,
    standalone, strictEqual, time, title, trk, trkpt, trkseg, type, version,
    write
*/

import assert from "assert";
import fs from "fs";
import {create} from "xmlbuilder2";

/**
 * If this looks familiar that's because it's binary search. We find
 * 0 <= i <= array.length such that !pred(i - 1) && pred(i) under the
 * assumption !pred(-1) && pred(length).
 */
function search(array, pred) {
    let le = -1;
    let ri = array.length;
    while (1 + le !== ri) {
        const mi = le + Math.floor((ri - le) / 2);
        if (pred(array[mi])) {
            ri = mi;
        } else {
            le = mi;
        }
    }
    return ri;
}

/**
 * Format a number of metres as a string.
 */
const formatDistance = (metres) => `${Math.round(metres / 100) / 10}km`;

if (4 !== process.argv.length) {
    console.error(`usage: ${process.argv[1]} index geojson

    Take an index and a GeoJSON file and produce GPX files for each item in
    the index.`);
    process.exit(64); // EX_USAGE
}

const index = JSON.parse(fs.readFileSync(process.argv[2], "utf8"));
const geojson = JSON.parse(fs.readFileSync(process.argv[3], "utf8"));

const gpxs = {};
assert.strictEqual(geojson.type, "FeatureCollection");
geojson.features.forEach(function (feature) {
    assert.strictEqual(feature.type, "Feature");
    const idx = search(
        index,
        (walk) => feature.properties.date < walk.dates[0]
    ) - 1;
    const walk = index[idx];
    if (-1 === walk.dates.indexOf(feature.properties.date)) {
        console.error("no index entry:", feature.properties.date);
        return;
    }
    const slug = walk.slug;
    if (!gpxs[slug]) {
        gpxs[slug] = {
            "@creator": "togpx",
            "@version": "1.1",
            "metadata": {
                "bounds": {
                    "@maxlat": feature.bbox[1 + feature.bbox.length / 2],
                    "@maxlon": feature.bbox[feature.bbox.length / 2],
                    "@minlat": feature.bbox[1],
                    "@minlon": feature.bbox[0]
                },
                "desc": `${walk.title}
Distance: ${formatDistance(walk.distances.reduce((a, i) => a + i))}
${walk.dates.join(", ")}
${walk.link}
`,
                "link": walk.link,
                "name": walk.title
            },
            "trk": []
        };
    }

    assert(
        "LineString" === feature.geometry.type ||
        "MultiLineString" === feature.geometry.type
    );
    const coordinates = (
        "LineString" === feature.geometry.type
        ? [feature.geometry.coordinates]
        : feature.geometry.coordinates
    );
    gpxs[slug].trk.push({
        "desc": `Distance: ${formatDistance(feature.properties.distance)}`,
        "time": feature.properties.date,
        "trkseg": coordinates.map((line) => ({
            "trkpt": line.map((coords) => ({
                "@lat": coords[1],
                "@lon": coords[0],
                "ele": (
                    2 < coords.length
                    ? [coords[2]]
                    : []
                )
            }))
        }))
    });
});

Object.keys(gpxs).forEach(function (slug) {
    const stream = fs.createWriteStream(`${slug}.gpx`);
    stream.write(create(
        {"encoding": "utf-8", "standalone": true, "version": "1.0"}
    ).ele(
        {"gpx@http://www.topografix.com/GPX/1/1": gpxs[slug]}
    ).end(
        {"prettyPrint": true}
    ));
    stream.end();
});
